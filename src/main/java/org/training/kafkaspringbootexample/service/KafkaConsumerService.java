package org.training.kafkaspringbootexample.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaConsumerService {

	@KafkaListener(topics = "test-message")
	public void consumer(String message) {
		log.info(String.format("Message received -> %s", message));
		
	}
}
