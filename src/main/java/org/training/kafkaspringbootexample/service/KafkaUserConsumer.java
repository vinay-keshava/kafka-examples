package org.training.kafkaspringbootexample.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.training.kafkaspringbootexample.payload.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaUserConsumer {

	@KafkaListener(topics = "cars-json-serialize")
	public void consume(User data) {
		log.info(String.format("Message Received -> %s", data.toString()));
	}
	
}
