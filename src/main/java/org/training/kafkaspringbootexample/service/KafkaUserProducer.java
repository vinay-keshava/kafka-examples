package org.training.kafkaspringbootexample.service;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.training.kafkaspringbootexample.payload.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaUserProducer {

	KafkaTemplate<String, User> kafkaTemplate;
	
	public KafkaUserProducer(KafkaTemplate<String, User> kafkaTemplate) {
		super();
		this.kafkaTemplate = kafkaTemplate;
	}

	public void sendMessage(User data) {
		log.info(String.format("Message sent -> %s",data.toString()));
		Message<User> message = MessageBuilder.withPayload(data).setHeader(KafkaHeaders.TOPIC, "cars-json-serialize")
				.build();
		
		kafkaTemplate.send(message);
	}
	
}
