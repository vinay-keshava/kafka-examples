package org.training.kafkaspringbootexample.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.kafkaspringbootexample.dto.ProducerDto;
import org.training.kafkaspringbootexample.payload.User;
import org.training.kafkaspringbootexample.service.KafkaProducerService;
import org.training.kafkaspringbootexample.service.KafkaUserProducer;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/api/v1")
@AllArgsConstructor
public class KafkaProducerController {

	
	private KafkaUserProducer kafkaUserProducer;
	
//	@PostMapping("/publish")
//    public ResponseEntity<String> publish(@RequestBody ProducerDto dto){
//		kafkaProducerService.sendMessage(dto.getMessage());
//        return ResponseEntity.ok("Message sent to kafka topic");
//    }
	
	@PostMapping("/publish")
    public ResponseEntity<String> publish(@RequestBody User user){
		kafkaUserProducer.sendMessage(user);
        return ResponseEntity.ok("Message sent to kafka topic");
    }
}
