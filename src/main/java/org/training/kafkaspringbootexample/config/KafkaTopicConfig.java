package org.training.kafkaspringbootexample.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfig {

	@Bean
	NewTopic createTopic() {
		return new NewTopic("cars-json-serialize", 2, (short) 1);

	}
    
//    @Bean
//    NewTopic createTopic()
//	{
//		return TopicBuilder.name("test-message").build();
//		
//	}
}
