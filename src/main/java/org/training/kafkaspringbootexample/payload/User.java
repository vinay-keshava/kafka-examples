package org.training.kafkaspringbootexample.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

	private Integer id;
	private String firstName;
	private String lastName;
}
